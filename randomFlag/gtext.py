# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

import gettext
import locale
import os

langs = [locale.getlocale()[0]]
localedir = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'messages')
translate = gettext.translation(
    'randomFlag', localedir, languages=langs, fallback=True)
_ = translate.gettext
