#!/usr/bin/python
# -*- mode: Python ; coding: utf-8 -*-
#
# Random flag generator
#
# © 2020–2023 Roland Sieker <ospalh@gmail.com>

from jinja2 import Template
import argparse
import colorsys
import drawSvg as draw
import json
import math
import os
import random
import re
import sys

from .descriptions import descriptions
from .gtext import _

def getOneWeighted(weighted_list):
    '''Global (well, file wide) method to pick one value from one of our
    three lists with probabilities'''
    values, weights = zip(*weighted_list)
    return random.choices(values, weights, k=1)[0]


class RandomFlag(object):
    '''A class to produce a random flag

    This produces an SVG or PNG image of a random flag, or of a flag
    based on a code that describes flags. The flags use simple
    geometric shapes, stripes, crosses, and circles, but no “charges”
    like stars, objects or logos. This comes with a list of known
    flags, most countries, constituent states &c. and usually avoids
    creating a known flag.

    '''

    aspect_ratios = [
        # There is a list.
        # https://en.wikipedia.org/wiki/List_of_aspect_ratios_of_national_flags
        # The last number is the probability for that value to be selected.
        (1, 10),  # Switzerland, but works well for 10.x and 1.x
        (15/13, 2),  # Belgium
        (7/6, 2),  # Niger
        (17/14, 2),  # Denmark (one version)
        (37/28, 1),  # Denmark (one version)
        (4/3, 20),  # Dem. rep. Congo
        (25/18, 3),  # Iceland
        (11/8, 3),  # Norway
        (1.4, 20),  # Albania
        (10/7, 5),  # Andorra
        (1.5, 50),  # Austria
        (1.6, 5),  # Argentine
        ((1+math.sqrt(5))/2, 2), # Togo, “golden ratio”
        (5/3, 10),  # Bahrain, Germany
        (1.75, 3),  # Mexico
        (39/19, 2),  # Vanuatu
        (1.9, 2),  # Micronesia, USA
        (2, 30),  # UK and North Korea
        (28/11, 0) # Qatar # About as silly as Nepal. I cut this off
                   # at 2:1. When the selection works correctly, this
                   # will never be pickt
    ]
    flag_generator_list = [
        # the list is code, number of colors, and tuple of
        # probabilities for the variants. The last number is the
        # probability for this flag code, again. For values >= 5 it is
        # how often i saw this in the known flags.
        (['1.1', 4, None], 7),
        (['1.2', 2, None], 5), # was 27, which is most of them. Not many left
        (['1.3', 2, None], 5), # was 24, basically the same
        (['1.4', 2, (10, 2, 0)], 5),
        (['1.5', 3, None], 5), # 3
        (['1.6', 3, None], 5), # 4
        (['1.7', 3, (5, 5, 0)], 11),
        (['1.8', 3, (1, 4, 0)], 5), # 5
        (['2.1', 3, (10, 0, 0, 2)], 82),
        (['2.2', 2, (10, 2, 2, 0)], 27),
        (['3.1', 3, None], 41),
        (['3.2', 2, (10, 2)], 16),
        (['4.1', 5, (10, 0, 10)], 5), # 1
        # The unused variant 1 is the larger offset from the quadrant
        # flags we use as the base.
        (['4.2', 3, (10, 0, 10)], 5),  # 3
        (['4.3', 3, (10, 0, 10)], 8),
        (['4.4', 3, (10, 0, 10)], 5),  # 5
        (['4.5', 2, (10, 0, 10)], 23),
        (['5.1', 6, (5, 0, 10)], 5),  # 0
        (['5.2', 4, (5, 0, 10)], 5),  # 3
        (['5.3', 4, (5, 0, 10)], 5),  # 2
        (['5.4', 4, (5, 0, 10)], 5),  # 2
        (['5.5', 3, (5, 0, 10)], 38),
        (['5.6', 2, (5, 0, 10)], 6),
        (['6.1', 4, (5, 2, 0.5, 5)], 14),
        (['6.2', 3, (5, 2, 0.5, 4)], 5),
        (['6.3', 2, (4, 10, 0, 4)], 5),  # 3
        (['6.4', 2, (4, 1, 0, 4)], 5),  # 0
        # Looks like a misshapen two-color three stripe flag
        (['6.5', 2, (10, 0, 2, 0)], 5),  # 0
        # Looks even worse. (We had this code below and didn’t use it)
        (['7.1', 3, (10, 5, 1)], 23),
        (['8.1', 3, (10, 2, 0)], 23),
        (['8.2', 2, (10, 2, 1)], 14),
        (['8.3', 3, (10, 2, 1)], 5),
        (['9.1', 4, None], 7),
        (['9.2', 3, None], 5), # 4
        (['9.3', 2, (1, 1)], 17),
        (['10.1', 4, None], 5), # 4
        (['10.2', 2, None], 13),
        (['10.3', 2, None], 22),
        (['10.4', 2, None], 7),
        (['10.5', 3, None], 5), # 0
        (['10.6', 3, None], 5), # 0
        (['11.1', 5, (10, 3, 0.5)], 5),  # 1
        (['11.2', 3, (10, 3, 0.5)], 5),  # 0
        (['11.3', 3, (10, 3, 0.5)], 5),  # 0
        (['11.4', 3, (10, 3, 0.5)], 6),
        (['11.5', 4, (10, 3, 0.5)], 5),  # 0
        (['11.6', 4, (10, 3, 0.5)], 5),  # 1
        (['11.7', 2, (10, 1, 2)], 17),
        (['12.1', 5, (10, 5, 1, 4)], 5),  # 2
        (['12.2', 4, (10, 5, 1, 4)], 5),  # 2
        (['12.3', 3, (5, 10, 0.2, 8)], 5),
        (['12.4', 4, (1, 0.5, 1, 10)], 5),  # 1
        (['12.5', 3, (1, 1, 1, 10)], 5),  # 3
        (['12.6', 2, (1, 3, 0.5, 0)], 5),  # 2
        (['12.7', 2, (1, 3, 0.5, 1)], 5),  # 0
        (['13.1', 4, (10, 1, 1, 1)], 5),  # 2
        (['14.1', 3, (10, 2, 0.5)], 24),
        (['14.2', 3, (10, 2, 0.5)], 16),
        (['14.3', 2, (10, 2, 0.5)], 12),
        (['14.4', 2, (10, 2, 0.5)], 12),
        (['15.1', 5, (3, 10, 0.5)], 5),  # 0
        (['15.2', 3, (3, 10, 0.5)], 5),  # 1
        (['15.3', 3, (3, 10, 0.5)], 5),  # 0
        (['15.4', 3, (3, 10, 0.5)], 5),  # 0
        (['15.5', 4, (3, 10, 0.5)], 5),  # 0
        (['15.6', 4, (3, 10, 0.5)], 5),  # 0
        (['15.7', 2, (5, 5, 0.5)], 7),
        (['16.1', 4, (5, 5, 1)], 8),
        (['16.2', 3, (5, 5, 1)], 5),  # 2
        (['16.3', 3, (5, 5, 5)], 5),  # 4
        (['16.4', 2, (5, 5, 5)], 5),  # 0
        (['17.1', 4, (5, 5)], 5),  # 0
        (['17.2', 2, (5, 5)], 5),  # 0
        (['17.3', 3, None], 8),
        (['17.4', 3, None], 5),
        (['18.1', 3, (10, 2, 0.5)], 5),
        (['18.2', 3, (10, 2, 0.5)], 5),
        (['19.1', 4, (10, 2)], 5),
        (['19.2', 4, (10, 2)], 5),
        (['19.3', 3, (15, 2)], 5),
        (['19.4', 3, (15, 2)], 5),
        # With gap between them
        (['19.5', 4, (10, 2)], 5),
        (['19.6', 4, (10, 2)], 5),
        (['19.7', 3, (15, 2)], 5),
        (['19.8', 3, (15, 2)], 5),
        # And the variants here
        (['20.1', 4, (1, 1, 10)], 5),
        (['20.2', 4, (1, 1, 10)], 5),
        (['20.3', 3, (10, 2, 2)], 5),
        (['20.4', 3, (10, 2, 2)], 5),
        (['20.5', 2, (15, 2, 2)], 5),
        (['20.6', 2, (15, 2, 2)], 5),
        # Like 19, but different diagonals
        (['21.1', 4, (10, 2)], 5),
        (['21.2', 4, (10, 2)], 5),
        (['21.3', 4, (10, 2)], 5),
        (['21.4', 4, (10, 2)], 5),
        (['21.5', 3, (10, 2)], 5),
        (['21.6', 3, (10, 2)], 5),
        (['22.1', 4, (1, 1, 10)], 5),
        (['22.2', 4, (1, 1, 10)], 5),
        (['23.1', 4, (10, 2, 0)], 23),
        (['23.2', 3, (10, 2, 1)], 14),
        (['23.3', 4, (10, 2, 1)], 5),
        (['23.4', 3, (10, 2, 1)], 2),
        (['23.5', 3, (10, 2, 1)], 2),
        (['23.6', 2, (10, 2, 1)], 2),
        (['23.7', 2, (10, 2, 1)], 2),
        (['24.1', 4, (10, 0, 4)], 2),
        (['24.2', 3, (10, 1, 2)], 14),
        (['24.3', 4, (10, 1, 2)], 5),
        (['24.4', 3, (10, 1, 2)], 2),
        (['24.5', 3, (10, 1, 4)], 2),
        (['24.6', 2, (10, 1, 4)], 2),
        (['24.7', 2, (10, 0, 4)], 2),
        (['25.1', 5, None], 5),
        (['25.2', 3, None], 2),
        (['25.3', 2, None], 2),
        (['25.4', 3, None], 2),
        (['25.5', 3, None], 2),
        (['25.6', 3, None], 2),
        (['26.1', 5, None], 5),
        (['26.2', 3, None], 2),
        (['26.3', 2, None], 2),
        (['26.4', 3, None], 2),
        (['26.5', 3, None], 2),
        (['26.6', 3, None], 2),
    ]
    colors_list = [
        # list is rgb, hsl start values, amount to fuzz the hsl, long
        # name and code, The last number is the probability for that
        # value, again. Actually how often i saw this color in the
        # known flags.
        (['black', (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), _('black'), 'k'], 170),
        (['white', (0.0, 1.0, 0.0), (0.0, 0.0, 0.0), _('white'), 'w'], 343),
        (['#cb0a2c', (0.971, 0.421, 0.9), (0.1, 0.1, 0.1), _('red'), 'r'],
         368),
        (['#1e742e', (0.364, 0.29, 0.581), (0.1, 0.1, 0.1), _('green'), 'g'],
         263),
        (['#254aa0', (0.617, 0.39, 0.618), (0.1, 0.1, 0.1), _('blue'), 'b'],
         323),
        (['#ffde01', (0.145, 0.501, 0.9), (0.1, 0.1, 0.1), _('yellow'), 'y'],
         282),
    ]


    def __init__(self, width, verbose, fuzz):
        this_dir, this_filename = os.path.split(__file__)
        self.drawing = None  # We only create the drawing when
                             # makeFlag() is calld. By then we know
                             # the size
        self.description = None
        self.verbose = verbose # Set this to False when everything works.
        self.fuzz_colors = fuzz
        self.fuzz_length = fuzz
        self.fuzz_variant = fuzz
        self.width = width
        self.length = 1.5 * self.width  # This may or may not be
                                        # overwritten later
        with open(os.path.join(this_dir, 'known_flags.json')) as kff:
            try:
                self.known_flags = json.load(kff)
            except:
                raise runtimeError('Could not read known flags.')
        self.colors = []
        self.flag_code = None
        self.color_code = None
        self.variant = 0
        # used to calculate how far to shift a diagonalstripe
        self.diagonal = math.sqrt(self.width*self.width+self.length*self.length)
        self.fuzzLength(False)



    def makePlainFlag(self, c_a):
        self.drawing = draw.Drawing(
            self.length, self.width, origin=(0, 0), displayInline=False)
        self.drawing.append(draw.Rectangle(
            0, 0, self.length, self.width, fill=c_a))


    def makeQuadrantFlag(self, c_a, c_b, c_c, c_d, quad_variant=None):
        '''Make flags of four quadrants, like bicolor flags (some German
        states) or like Panama’s'''
        self.makeHorizontalBicolor(c_a, c_c)
        xp = 1/2
        # We need a local variant for the lozenge flags, where we use
        # the variant in a different meaning (size of lozenge, not
        # horizontal position of cut)
        if quad_variant is None:
            quad_variant = self.variant
        if quad_variant == 1:
            xp = 1/3
        if quad_variant == 2:
            xp = 3/8  # This is where the center of the cros on Swedish flags is.
        self.drawing.append(draw.Rectangle(
            xp*self.length, self.width/2, (1-xp) * self.length, self.width/2,
            fill=c_b))
        self.drawing.append(draw.Rectangle(
            xp*self.length, 0, (1-xp) * self.length, self.width/2, fill=c_d))


    def makeVerticalBicolor(self, c_a, c_b, x_variant=None):
        self.makePlainFlag(c_a)
        xp = 1/2
        # We need a local variant for the lozenge flags, where we use
        # the variant in a different meaning (size of lozenge, not
        # horizontal position of cut)
        if x_variant is None:
            x_variant = self.variant
        if x_variant == 1:
            xp = 1/3
        if x_variant == 2:
            xp = 3/8  # This is where the center of the cros on Swedish flags is.
        self.drawing.append(draw.Rectangle(
            xp*self.length, 0, (1-xp) * self.length, self.width, fill=c_b))


    def makeHorizontalBicolor(self, c_a, c_b, variant=None):
        self.makePlainFlag(c_a)
        self.drawing.append(draw.Rectangle(
            0, 0, self.length, self.width/2, fill=c_b))


    def makeHorizontalTriband(self, c_a, c_b, c_c, stripe_variant=None):
        '''Make a flag like those of Germany, Austria or Colombia'''
        self.makePlainFlag(c_a)
        # See comment for chevron triband
        if stripe_variant is None:
            stripe_variant = self.variant
        yp1 = 1/3
        yp2 = 2/3
        if stripe_variant == 1:
            yp1 = 2/5
            yp2 = 3/5
        if stripe_variant == 2:
            yp1 = 1/4
            yp2 = 3/4
        if stripe_variant == 3:
            yp1 = 1/2
            yp2 = 3/4
        self.drawing.append(draw.Rectangle(
            0, yp1 * self.width, self.length, (yp2-yp1) * self.width, fill=c_b))
        self.drawing.append(draw.Rectangle(
            0, 0, self.length, yp1 * self.width, fill=c_c))


    def makeVerticalTriband(self, c_a, c_b, c_c):
        '''Make a flag like those of France or Peru'''
        self.makePlainFlag(c_a)
        xp1 = 1/3
        xp2 = 2/3
        if self.variant == 1:
            xp1 = 1/4
            xp2 = 3/4
        self.drawing.append(draw.Rectangle(
            xp1*self.length, 0, (xp2-xp1) * self.length, self.width, fill=c_b))
        self.drawing.append(draw.Rectangle(
            xp2 * self.length, 0, (1-xp2) * self.length, self.width, fill=c_c))


    def addCros(self, w, ci):
        '''Add a cros of length w and color ci to self.drawing'''
        xp = self.length/2
        if self.variant == 1:
            xp = 1/3 * self.length
        if self.variant == 2:
            xp = 3/8 * self.length # This is where the center of the
                                  # cros on Swedish flags is.  Make
                                  # sure to keep this in line with
                                  # variant 2 of the quadrat flag.
        self.drawing.append(draw.Line(
            0, self.width/2, self.length, self.width/2, stroke=ci,
            stroke_width=w, fill='none'))
        self.drawing.append(draw.Line(
            xp, 0, xp, self.width, stroke=ci, stroke_width=w, fill='none'))


    def makeCrosFlag(self, c_a, c_b, c_c, c_d, c_e):
        '''Make a flag like those of Swedish, English or Saar Protectorate.'''
        # nordic_crosbeam_width = 1/5  # Swedish
        beam_width = 1/6  # So use something between Denmark and Sweden
        # nordic_crosbeam_width = 1/7  # Danish
        self.makeQuadrantFlag(c_a, c_b, c_c, c_d)
        self.addCros(beam_width*self.width, c_e)


    def makeBorderdCrosFlag(self, c_a, c_b, c_c, c_d, c_e, c_f):
        '''Make a flag like those of Swedish, English or Saar Protectorate.'''
        inner_beam_width = 1/8 # Norway. We draw the Danish/Swedish cros
        # first and later cover parts of it with the border. We need the
        # length to determine the position.
        beam_border_width = 1/16 # Norway
        total_border_width = inner_beam_width + 2* beam_border_width
        self.makeQuadrantFlag(c_a, c_b, c_c, c_d)
        # There are a few points where we want makeQuadrantFlag with
        # variant 0. This is not one of them. Use self.variant there,
        # as well.
        self.addCros(total_border_width*self.width, c_f)
        self.addCros(inner_beam_width*self.width, c_e)


    def addChevron(self, xp, ci):
        self.drawing.append(draw.Lines(
            0, 0,
            xp, self.width/2,
            0, self.width,
            stroke='none', stroke_width=0, close=True, fill=ci))


    def addBluntChevron(self, xp, ci):
        self.drawing.append(draw.Lines(
            0, 0,
            xp, self.width/3,
            xp, 2*self.width/3,
            0, self.width,
            stroke='none', stroke_width=0, close=True, fill=ci))


    def makeChevronTribandFlag(self, c_a, c_b, c_c, c_d):
        # This is a special case. We want the basic tricol to be
        # variant 0, i.e. equal width stripse, but use the variant for
        # other things, the size and shape of the chevrons.
        self.makeHorizontalTriband(c_a, c_b, c_c, stripe_variant=0)
        xp = self.length/2
        if 1 == self.variant:
            xp = self.length
        if 2 == self.variant:
            xp = self.length/5
        if 3 == self.variant:
            xp = self.length/3
            self.addBluntChevron(xp, c_d)
            return
        # Still here: variant 0 or 1 (or something odd)
        self.addChevron(xp, c_d)

    def makeChevronBicolorFlag(self, c_a, c_b, c_c):
        '''Make a flag like the Czech one'''
        # For this, choosing the wrong variant for the quadrant flag
        # would be no problem. Use the correct one aniway.
        self.makeHorizontalBicolor(c_a, c_b)
        xp = self.length/2
        if 1 == self.variant:
            xp = self.length
        if 2 == self.variant:
            xp = self.length/5
        self.addChevron(xp, c_c)


    def makeFiveStripeFlag(self, c_a, c_b, c_c, c_d):
        '''Make a flag like some pride flags'''
        self.drawing = draw.Drawing(
            self.length, self.width, origin=(0, 0), displayInline=False)
        yp1 = 1/5
        yp2 = 2/5
        if 1 == self.variant:
            yp1 = 1/4
            yp2 = 3/8
        if 2 == self.variant:
            yp1 = 1/6
            yp2 = 1/3
        self.drawing.append(draw.Rectangle(
            0, 0, self.length, yp1 * self.width, fill=c_a))
        self.drawing.append(draw.Rectangle(
            0, yp1*self.width, self.length, (yp2-yp1) * self.width, fill=c_d))
        self.drawing.append(draw.Rectangle(
            0, yp2*self.width, self.length, (1-2*yp2) * self.width, fill=c_c))
        self.drawing.append(draw.Rectangle(
            0, (1-yp2)*self.width, self.length, (yp2-yp1) * self.width, fill=c_b))
        self.drawing.append(draw.Rectangle(
            0, (1-yp1)*self.width, self.length, yp1 * self.width, fill=c_a))


    def makeCircleTriband(self, c_a, c_b, c_c, c_d):
        circle_radius = 0.35 * self.width
        xo = 0.5 * self.length
        if 1 == self.variant:
            xo = 0.4 * self.length
        self.makeHorizontalTriband(c_a, c_b, c_c)
        self.drawing.append(draw.Circle(
            xo, self.width/2, circle_radius, fill=c_d))


    def makeDiagonalFlag(self, c_a, c_b, sinister=True):
        self.drawing = draw.Drawing(
            self.length, self.width, origin=(0, 0), displayInline=False)
        # Can we set the background color? Maybe. But this should work.
        self.drawing.append(draw.Rectangle(
            0, 0, self.length, self.width, fill=c_b))
        if sinister:
            self.drawing.append(draw.Lines(
                0, 0,
                self.length, self.width,
                0, self.width,
                stroke='none', stroke_width=0, close=True, fill=c_a))
        else:
            self.drawing.append(draw.Lines(
                0, self.width,
                self.length, 0,
                self.length, self.width,
                stroke='none', stroke_width=0, close=True, fill=c_a))


    def makeSaltireFourcolorFlag(self, c_a, c_b, c_c, c_d):
        self.makeDiagonalFlag(c_a, c_c, sinister=True)
        self.drawing.append(draw.Lines(
            0, 0,
            self.length/2, self.width/2,
            0, self.width,
            stroke='none', stroke_width=0, close=True, fill=c_b))
        self.drawing.append(draw.Lines(
            0, 0,
            self.length/2, self.width/2,
            self.length, 0,
            stroke='none', stroke_width=0, close=True, fill=c_d))

    def addSaltireCros(self, ci):
        w = self.width/6
        if 1 == self.variant:
            w = self.width/12
        if 2 == self.variant:
            w = self.width/3
        self.drawing.append(draw.Line(
            0, 0, self.length, self.width, stroke=ci, stroke_width=w,
            fill='none'))
        self.drawing.append(draw.Line(
            self.length, 0, 0, self.width, stroke=ci, stroke_width=w,
            fill='none'))


    def makeSaltireFivecolorFlag(self, c_a, c_b, c_c, c_d, c_e):
        self.makeSaltireFourcolorFlag(c_a, c_b, c_c, c_d)
        self.addSaltireCros(ci=c_e)


    def addLineChevron(self, xp, w, ci):
        self.drawing.append(draw.Lines(
            0, 0,
            xp, self.width/2,
            0, self.width,
            stroke_width=w, stroke=ci,
            fill='none'))


    def makeDoubleChevronTribandFlag(self, c_a, c_b, c_c, c_d, c_e):
        self.makeHorizontalTriband(c_a, c_b, c_c, stripe_variant=0)
        # See single chevron triband comment above
        xp1 = self.length/2
        xp2 = self.length/4
        if 1 == self.variant:
            xp1 = self.length
            xp2 = self.length/2
        if 2 == self.variant:
            xp1 = self.length/5
            xp2 = self.length/10
        if 3 == self.variant:
            w = self.width/4
            xp2 = self.length/2
            self.addChevron(xp2, c_d)
            self.addLineChevron(xp2-w/2+1, w, c_e)
            return
        # Still here: variant 0 or 1 (or something odd)
        self.addChevron(xp1, c_e)
        self.addChevron(xp2, c_d)


    def makeDoubleChevronBicolorFlag(self, c_a, c_b, c_c, c_d):
        self.makeHorizontalBicolor(c_a, c_b)
        # See variant remarks above
        xp1 = self.length/2
        xp2 = self.length/4
        if 1 == self.variant:
            xp1 = self.length
            xp2 = self.length/2
        if 2 == self.variant:
            xp1 = self.length/5
            xp2 = self.length/8
        if 3 == self.variant:
            w = self.width/4
            xp2 = self.length/2
            self.addChevron(xp2, c_c)
            self.addLineChevron(xp2-w/2+1, w, c_d)
            return
        # Still here: variant 0 or 1 (or something odd)
        self.addChevron(xp1, c_d)
        self.addChevron(xp2, c_c)


    def addBend(self, ci, bend_width, sinister=True, offset=0, half=0):
        x_off = offset * self.width/self.diagonal
        y_off = offset * self.length/self.diagonal
        if sinister:
            left_y = 0
            right_y = self.width
        else:
            x_off = -x_off
            left_y = self.width
            right_y = 0
        left_x = 0 - x_off
        right_x = self.length - x_off
        if half == -1:
            right_x = self.length/2
            right_y = self.width/2
        if half == 1:
            left_x = self.length/2
            left_y = self.width/2
        self.drawing.append(draw.Line(
            left_x, left_y + y_off, right_x, right_y + y_off, stroke=ci,
            stroke_width=bend_width, fill='none'))


    def makeBendFlag(self, c_a, c_b, c_c, sinister=True, reverse_halves=False):
        # This could be just an xor in the makeDiagonalFlag. This is
        # longer but readable.
        w = self.width/5
        if 1 == self.variant:
            w = self.width/12
        if 2 == self.variant:
            w = 0.6 * self.width
        if reverse_halves:
            self.makeDiagonalFlag(c_a, c_c, not sinister)
        else:
            self.makeDiagonalFlag(c_a, c_c, sinister)
        self.addBend(c_b, w, sinister=sinister)


    def makeDoubleBendFlag(
            self, c_a, c_b, c_c, c_d, sinister=True, reverse_halves=False, gap=False):
        # This could be just an xor in the makeDiagonalFlag. This is
        # longer but readable.
        w = self.width/7
        if 1 == self.variant or 3 == self.variant:
            w = self.width/15
        o = 0
        if gap:
            o = w/2
        if reverse_halves:
            self.makeDiagonalFlag(c_a, c_d, not sinister)
        else:
            self.makeDiagonalFlag(c_a, c_d, sinister)
        self.addBend(c_b, bend_width=w, sinister=sinister, offset=w/2+o, half=0)
        self.addBend(c_c, bend_width=w*1.005, sinister=sinister, offset=-w/2-o, half=0)


    def makeTripleBendFlag(
            self, c_a, c_b, c_c, c_d, c_e, sinister=True, reverse_halves=False):
        # This could be just an xor in the makeDiagonalFlag. This is
        # longer but readable.
        w_o = self.width/6
        w_i = self.width/6
        if 1 == self.variant:
            w_o = self.width/10
            w_i = self.width/10
        if 2 == self.variant:
            w_o = self.width/15
            w_i = self.width/6
        if reverse_halves:
            self.makeDiagonalFlag(c_a, c_e, not sinister)
        else:
            self.makeDiagonalFlag(c_a, c_e, sinister)
        # We draw the inner stripe first and a bit too wide to avoid
        # gap artifacts
        self.addBend(c_c, bend_width=w_i+0.25*w_o, sinister=sinister, offset=0)
        self.addBend(c_b, bend_width=w_o, sinister=sinister, offset=w_i/2+w_o/2)

        self.addBend(c_d, bend_width=w_o, sinister=sinister, offset=-w_i/2-w_o/2)


    def addLozenge(self, ci):
        o = 0
        if 1 == self.variant:
            o = 1/8
        if 2 == self.variant:
            o = -1/3
        self.drawing.append(draw.Lines(
            o*self.length, self.width/2,
            self.length/2, (1-o) * self.width,
            (1-o) * self.length, self.width/2,
            self.length/2, o * self.width,
            stroke='none', stroke_width=0, close=True, fill=ci))


    def makeCentralLozengeFlag(self, c_a, c_b, c_c, c_d, c_e):
        self.makeQuadrantFlag(c_a, c_b, c_c, c_d, quad_variant=0)
        # Like the three stripe chevron flag, we need variant 0 for
        # the base flag here.
        self.addLozenge(ci=c_e)


    def makeBardTriband(self, c_a, c_b, c_c, c_d):
        w = 1/4 * self.length
        if 1 == self.variant:
            w = 1/3 * self.length
        if 2 == self.variant:
            w = 1/6 * self.length
        self.makeHorizontalTriband(c_a, c_b, c_c, stripe_variant=0)
        # See variant comments above
        self.drawing.append(draw.Rectangle(0, 0, w, self.width, fill=c_d))


    def makeSplitCircleFlag(self, c_a, c_b, c_c, c_d):
        r = 1/3 * self.width  # Greenlandic
        c = self.length/2
        if 1 == self.variant:
            c = 7/18 * self.length  # also Greenlandic
        self.makeHorizontalBicolor(c_a, c_d)
        # One more “local variant is nice, but not strictly necessary”
        # case.
        self.drawing.append(draw.Circle(
            c, self.width/2, r, fill=c_b))
        self.drawing.append(draw.Arc(
            c, self.width/2, r, 180, 360, stroke_width=0, stroke='none',
            fill=c_c))


    def makeBardFiveStripeFlag(self, c_a, c_b, c_c, c_d, c_e):
        w = 1/4 * self.length
        if 1 == self.variant:
            w = 1/3 * self.length
        if 2 == self.variant:
            w = 1/6 * self.length
        self.makeFiveStripeFlag(c_a, c_b, c_c, c_d)
        # See variant comments above
        self.drawing.append(draw.Rectangle(0, 0, w, self.width, fill=c_e))


    def makeFiveStripeFlagWithTriangle(self, c_a, c_b, c_c, c_d, c_e):
        self.makeFiveStripeFlag(c_a, c_b, c_c, c_d)
        xp = self.length/2
        if self.variant == 1:
            xp = self.length
        if self.variant == 2:
            xp = self.length/5
        self.addChevron(xp, c_e)

    def makeFiveRayFlag(self, c_a, c_b, c_c, c_d, c_e, fromBottomHoist):
        self.makePlainFlag(c_c)
        w_offset = self.width
        w_factor = -1
        if fromBottomHoist:
            w_offset = 0
            w_factor = 1
        # Now add the four other stripes
        # We draw them as much too large triangles thru the correct corner
        # Two,
        self.drawing.append(draw.Lines(
            -2/3*self.length, w_offset - w_factor * self.width,
            +2 * 2/3*self.length, w_offset + 2 * w_factor * self.width,
            -2/3*self.length, w_offset + 2 *  w_factor * self.width,
            stroke='none', stroke_width=0, close=True, fill=c_b))
        # One
        self.drawing.append(draw.Lines(
            -1/3*self.length, w_offset - w_factor * self.width,
            + 2 * 1/3*self.length, w_offset + 2 * w_factor * self.width,
            -1/3*self.length, w_offset + 2 *  w_factor * self.width,
            stroke='none', stroke_width=0, close=True, fill=c_a))
        # Four
        self.drawing.append(draw.Lines(
            -self.length, w_offset - w_factor * 2/3*self.width,
            2 * self.length, w_offset + w_factor * 2 * 2/3*self.width,
            2 * self.length, w_offset - w_factor * 2/3*self.width,
            stroke='none', stroke_width=0, close=True, fill=c_d))
        # Five
        self.drawing.append(draw.Lines(
            -self.length, w_offset - w_factor * 1/3*self.width,
            2 * self.length, w_offset + w_factor * 2 * 1/3*self.width,
            2 * self.length, w_offset - w_factor * 1/3*self.width,
            stroke='none', stroke_width=0, close=True, fill=c_e))



    def makeFlagFromCode(self):
        '''Call the right flag generation function based on the code'''
        rgbc, dummy_hsl, dummy_pm, color_names, dummy_cc = zip(*self.colors)
        # if self.verbose:
        #     print(f'color names: {color_names}', file=sys.stderr)
        fc = self.flag_code
        # Just to save tiping So, yah, this is
        # an effing long list. But this information has to be
        # somewhere, and might as well be here. The point is, of
        # course, that the first part of the code is the function to
        # call (mostly) and the second part is how to distribute the
        # colors, and how many different colors to actually use. This
        # is wholly arbitrary. I made notes on paper, and this is
        # those notes turned into code.
        if fc == '1.1':
            self.makeQuadrantFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[3])
        if fc == '1.2':
            self.makeHorizontalBicolor(rgbc[0], rgbc[1])
        if fc == '1.3':
            self.makeVerticalBicolor(rgbc[0], rgbc[1])
        if fc == '1.4':
            self.makeQuadrantFlag(rgbc[0], rgbc[1], rgbc[1], rgbc[0])
        if fc == '1.5':
            self.makeQuadrantFlag(rgbc[0], rgbc[1], rgbc[1], rgbc[2])
        if fc == '1.6':
            self.makeQuadrantFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[0])
        if fc == '1.7':
            self.makeQuadrantFlag(rgbc[0], rgbc[1], rgbc[0], rgbc[2])
        if fc == '1.8':
            self.makeQuadrantFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[2])
        if fc == '2.1':
            self.makeHorizontalTriband(rgbc[0], rgbc[1], rgbc[2])
        if fc == '2.2':
            self.makeHorizontalTriband(rgbc[0], rgbc[1], rgbc[0])
        if fc == '3.1':
            self.makeVerticalTriband(rgbc[0], rgbc[1], rgbc[2])
        if fc == '3.2':
            self.makeVerticalTriband(rgbc[0], rgbc[1], rgbc[0])
        if fc == '4.1':
            self.makeCrosFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[3], rgbc[4])
        if fc == '4.2':
            self.makeCrosFlag(rgbc[0], rgbc[0], rgbc[1], rgbc[1], rgbc[2])
        if fc == '4.3':
            self.makeCrosFlag(rgbc[0], rgbc[1], rgbc[1], rgbc[0], rgbc[2])
        if fc == '4.4':
            self.makeCrosFlag(rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[2])
        if fc == '4.5':
            self.makeCrosFlag(rgbc[0], rgbc[0], rgbc[0], rgbc[0], rgbc[1])
        if fc == '5.1':
            self.makeBorderdCrosFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3], rgbc[4], rgbc[5])
        if fc == '5.2':
            self.makeBorderdCrosFlag(
                rgbc[0], rgbc[0], rgbc[1], rgbc[1], rgbc[2], rgbc[3])
        if fc == '5.3':
            self.makeBorderdCrosFlag(
                rgbc[0], rgbc[1], rgbc[1], rgbc[0], rgbc[2], rgbc[3])
        if fc == '5.4':
            self.makeBorderdCrosFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[2], rgbc[3])
        if fc == '5.5':
            self.makeBorderdCrosFlag(
                rgbc[0], rgbc[0], rgbc[0], rgbc[0], rgbc[1], rgbc[2])
        if fc == '5.6':
            self.makeBorderdCrosFlag(
                rgbc[0], rgbc[0], rgbc[0], rgbc[0], rgbc[0], rgbc[1])
        if fc == '6.1':
            self.makeChevronTribandFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[3])
        if fc == '6.2':
            self.makeChevronTribandFlag(rgbc[0], rgbc[1], rgbc[0], rgbc[2])
        if fc == '6.3':
            self.makeChevronTribandFlag(rgbc[0], rgbc[0], rgbc[0], rgbc[1])
        if fc == '6.4':
            self.makeChevronTribandFlag(rgbc[0], rgbc[1], rgbc[0], rgbc[1])
        if fc == '6.5':
            self.makeChevronTribandFlag(rgbc[0], rgbc[1], rgbc[0], rgbc[0])
        if fc == '7.1':
            self.makeChevronBicolorFlag(rgbc[0], rgbc[1], rgbc[2])
        if fc == '8.1':
            self.makeFiveStripeFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[1])
        if fc == '8.2':
            self.makeFiveStripeFlag(rgbc[0], rgbc[1], rgbc[0], rgbc[1])
        if fc == '8.3':
            self.makeFiveStripeFlag(rgbc[0], rgbc[1], rgbc[0], rgbc[2])
        if fc == '9.1':
            self.makeCircleTriband(rgbc[0], rgbc[1], rgbc[2], rgbc[3])
        if fc == '9.2':
            self.makeCircleTriband(rgbc[0], rgbc[1], rgbc[0], rgbc[2])
        if fc == '9.3':
            self.makeCircleTriband(rgbc[0], rgbc[0], rgbc[0], rgbc[1])
        if fc == '10.1':
            self.makeSaltireFourcolorFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[3])
        if fc == '10.2':
            # These still count as four-quadrant, but got their own function.
            self.makeDiagonalFlag(rgbc[0], rgbc[1], sinister=True)
        if fc == '10.3':
            self.makeDiagonalFlag(rgbc[0], rgbc[1], sinister=False)
        if fc == '10.4':
            self.makeSaltireFourcolorFlag(rgbc[0], rgbc[1], rgbc[1], rgbc[0])
        if fc == '10.5':
            self.makeSaltireFourcolorFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[0])
        if fc == '10.6':
            self.makeSaltireFourcolorFlag(rgbc[0], rgbc[1], rgbc[1], rgbc[2])
        if fc == '11.1':
            self.makeSaltireFivecolorFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3], rgbc[4])
        if fc == '11.2':
            self.makeSaltireFivecolorFlag(
                rgbc[0], rgbc[0], rgbc[1], rgbc[1], rgbc[2])
        if fc == '11.3':
            self.makeSaltireFivecolorFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[2])
        if fc == '11.4':
            self.makeSaltireFivecolorFlag(
                rgbc[0], rgbc[1], rgbc[1], rgbc[0], rgbc[2])
        if fc == '11.5':
            self.makeSaltireFivecolorFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[0], rgbc[3])
        if fc == '11.6':
            self.makeSaltireFivecolorFlag(
                rgbc[0], rgbc[1], rgbc[1], rgbc[2], rgbc[3])
        if fc == '11.7':
            self.makeSaltireFivecolorFlag(
                rgbc[0], rgbc[0], rgbc[0], rgbc[0], rgbc[1])
        if fc == '12.1':
            self.makeDoubleChevronTribandFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3], rgbc[4])
        if fc == '12.2':
            self.makeDoubleChevronTribandFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[2],  rgbc[3])
        if fc == '12.3':
            self.makeDoubleChevronTribandFlag(
                rgbc[0], rgbc[0], rgbc[0], rgbc[1],  rgbc[2])
        if fc == '12.4':
            self.makeDoubleChevronTribandFlag(
                rgbc[0], rgbc[1], rgbc[2],  rgbc[3], rgbc[1])
        if fc == '12.5':
            self.makeDoubleChevronTribandFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[2],  rgbc[1])
        if fc == '12.6':
            self.makeDoubleChevronTribandFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[0], rgbc[1])
        if fc == '12.7':
            self.makeDoubleChevronTribandFlag(
                rgbc[0], rgbc[0], rgbc[0], rgbc[0], rgbc[1])
        if fc == '13.1':
            self.makeDoubleChevronBicolorFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3])
        if fc == '14.1':
            self.makeBendFlag(rgbc[0], rgbc[1], rgbc[2])
        if fc == '14.2':
            self.makeBendFlag(rgbc[0], rgbc[1], rgbc[2], sinister=False)
        if fc == '14.3':
            self.makeBendFlag(rgbc[0], rgbc[1], rgbc[0])
        if fc == '14.4':
            self.makeBendFlag(rgbc[0], rgbc[1], rgbc[0], sinister=False)
        if fc == '15.1':
            self.makeCentralLozengeFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3], rgbc[4])
        if fc == '15.2':
            self.makeCentralLozengeFlag(
                rgbc[0], rgbc[0], rgbc[1], rgbc[1], rgbc[2])
        if fc == '15.3':
            self.makeCentralLozengeFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[2])
        if fc == '15.4':
            self.makeCentralLozengeFlag(
                rgbc[0], rgbc[1], rgbc[1], rgbc[0], rgbc[2])
        if fc == '15.5':
            self.makeCentralLozengeFlag(
                rgbc[0], rgbc[1], rgbc[1], rgbc[2], rgbc[3])
        if fc == '15.6':
            self.makeCentralLozengeFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[0], rgbc[3])
        if fc == '15.7':
            self.makeCentralLozengeFlag(
                rgbc[0], rgbc[0], rgbc[0], rgbc[0], rgbc[1])
        if fc == '16.1':
            self.makeBardTriband(rgbc[0], rgbc[1], rgbc[2], rgbc[3])
        if fc == '16.2':
            self.makeBardTriband(rgbc[0], rgbc[1], rgbc[0], rgbc[2])
        if fc == '16.3':
            self.makeBardTriband(rgbc[0], rgbc[1], rgbc[2], rgbc[1])
        if fc == '16.4':
            self.makeBardTriband(rgbc[0], rgbc[1], rgbc[0], rgbc[1])
        if fc == '17.1':
            self.makeSplitCircleFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[3])
        if fc == '17.2':
            self.makeSplitCircleFlag(rgbc[0], rgbc[1], rgbc[0], rgbc[1])
        if fc == '17.3':
            self.makeSplitCircleFlag(rgbc[0], rgbc[1], rgbc[1], rgbc[2])
        if fc == '17.4':
            self.makeSplitCircleFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[0])
        if fc == '18.1':
            self.makeBendFlag(rgbc[0], rgbc[1], rgbc[2], reverse_halves=True)
        if fc == '18.2':
            self.makeBendFlag(
                rgbc[0], rgbc[1], rgbc[2], sinister=False, reverse_halves=True)
        if fc == '19.1':
            self.makeDoubleBendFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[3])
        if fc == '19.2':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3], sinister=False)
        if fc == '19.3':
            self.makeDoubleBendFlag(rgbc[0], rgbc[1], rgbc[2], rgbc[0])
        if fc == '19.4':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[0], sinister=False)
        if fc == '19.5':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3], gap=True)
        if fc == '19.6':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3], sinister=False, gap=True)
        if fc == '19.7':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[0], gap=True)
        if fc == '19.8':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[0], sinister=False, gap=True)
        if fc == '20.1':
            self.makeTripleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[3])
        if fc == '20.2':
            self.makeTripleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[3], sinister=False)
        if fc == '20.3':
            self.makeTripleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[0])
        if fc == '20.4':
            self.makeTripleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[0], sinister=False)
        if fc == '20.5':
            self.makeTripleBendFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[0])
        if fc == '20.6':
            self.makeTripleBendFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[0], sinister=False)
        if fc == '21.1':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3], reverse_halves=True)
        if fc == '21.2':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3],
                sinister=False, reverse_halves=True)
        if fc == '21.3':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3],
                gap=True, reverse_halves=True)
        if fc == '21.4':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3],
                sinister=False, gap=True, reverse_halves=True)
        if fc == '21.5':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[1], rgbc[2],
                gap=True, reverse_halves=True)
        if fc == '21.6':
            self.makeDoubleBendFlag(
                rgbc[0], rgbc[1], rgbc[1], rgbc[2],
                sinister=False, gap=True, reverse_halves=True)
        if fc == '22.1':
            self.makeTripleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[3],
                reverse_halves=True)
        if fc == '22.2':
            self.makeTripleBendFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[3],
                sinister=False, reverse_halves=True)
        if fc == '23.1':
            self.makeBardFiveStripeFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[3])
        if fc == '23.2':
            self.makeBardFiveStripeFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[2])
        if fc == '23.3':
            self.makeBardFiveStripeFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[2], rgbc[3])
        if fc == '23.4':
            self.makeBardFiveStripeFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[2])
        if fc == '23.5':
            self.makeBardFiveStripeFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[1])
        if fc == '23.6':
            self.makeBardFiveStripeFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[0])
        if fc == '23.7':
            self.makeBardFiveStripeFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[1])
        if fc == '24.1':
            self.makeFiveStripeFlagWithTriangle(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[3])
        if fc == '24.2':
            self.makeFiveStripeFlagWithTriangle(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[2])
        if fc == '24.3':
            self.makeFiveStripeFlagWithTriangle(
                rgbc[0], rgbc[1], rgbc[0], rgbc[2], rgbc[3])
        if fc == '24.4':
            self.makeFiveStripeFlagWithTriangle(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[2])
        if fc == '24.5':
            self.makeFiveStripeFlagWithTriangle(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[1])
        if fc == '24.6':
            self.makeFiveStripeFlagWithTriangle(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[0])
        if fc == '24.7':
            self.makeFiveStripeFlagWithTriangle(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[1])
        if fc == '25.1':
            self.makeFiveRayFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[3], rgbc[4], True)
        if fc == '25.2':
            self.makeFiveRayFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[0], True)
        if fc == '25.3':
            self.makeFiveRayFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[0], True)
        if fc == '25.4':
            self.makeFiveRayFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[2], rgbc[0], True)
        if fc == '25.5':
            self.makeFiveRayFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[2], rgbc[2], True)
        if fc == '25.6':
            self.makeFiveRayFlag(
                rgbc[2], rgbc[2], rgbc[2], rgbc[1], rgbc[0], True)
        if fc == '26.1':
            self.makeFiveRayFlag(
                rgbc[4], rgbc[3], rgbc[2], rgbc[1], rgbc[0], False)
        if fc == '26.2':
            self.makeFiveRayFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[1], rgbc[0], False)
        if fc == '26.3':
            self.makeFiveRayFlag(
                rgbc[0], rgbc[1], rgbc[0], rgbc[1], rgbc[0], False)
        if fc == '26.4':
            self.makeFiveRayFlag(
                rgbc[0], rgbc[2], rgbc[0], rgbc[1], rgbc[0], False)
        if fc == '26.5':
            self.makeFiveRayFlag(
                rgbc[0], rgbc[1], rgbc[2], rgbc[2], rgbc[2], False)
        if fc == '26.6':
            self.makeFiveRayFlag(
                rgbc[2], rgbc[2], rgbc[2], rgbc[1], rgbc[0], False)


    def fuzzColors(self):
        for a_color in self.colors:
            h, l, s = a_color[1]
            hr, lr, sr = a_color[2]
            h = random.triangular(h-hr/2, h+hr/2) % 1.0
            l = random.triangular(l-lr/2, l+lr/2)
            s = random.triangular(s-sr/2, s+sr/2)
            r, g, b = colorsys.hls_to_rgb(h, l, s)
            a_color[0] = '#{:02x}{:02x}{:02x}'.format(
                int(r*255), int(g*255), int(b*255))


    def fuzzLength(self, do_fuzz=True):
        # Fuzz or set length
        if do_fuzz:
            self.length = int(self.width * getOneWeighted(RandomFlag.aspect_ratios))
        self.diagonal = math.sqrt(self.width*self.width+self.length*self.length)


    def getFlagCodes(self):
        '''Get longer flag codes, including the number of colors

        Depending on self.flag_code, this either gets the long flag
        code for the short code we put in, or gets one random long
        flag code.
        '''
        if self.flag_code:
            for fc, fw in RandomFlag.flag_generator_list:
                if fc[0] == self.flag_code:
                    return fc
            raise RuntimeError(f'Flag code {self.flag_code} unknown')
        else:
            fc = getOneWeighted(RandomFlag.flag_generator_list)
            self.flag_code = fc[0]
            return fc


    def getColors(self, count):
        self.colors = []
        if self.color_code and len(self.color_code) < count:
            raise ValueError(
                f'Give at least {count} colors for this flag type, or 6')
        if count > len(RandomFlag.colors_list):
            raise RuntimeError("Requested too many colors")
        if self.color_code:
            for color_letter in self.color_code:
                for a_color, color_prop in RandomFlag.colors_list:
                    if a_color[4] == color_letter:
                        self.colors.append(a_color)
        else:
            self.color_code = []
            for dummy_i in range(count):
                while True:
                    # Should not take more than something like 10 tries at most
                    a_color = getOneWeighted(RandomFlag.colors_list)
                    if a_color not in self.colors:
                        self.colors.append(a_color)
                        self.color_code.append(a_color[4])
                        break
        if self.fuzz_colors:
            self.fuzzColors()


    def setFlagInfo(self, code, avoid_known):
        '''Determine which flag we should make.'''
        self.flag_code = None
        self.color_code = None
        self.description = None
        self.variant = 0
        user_variant = False  # I ges i could use the difference
                              # between 0 and None here. This is
                              # probably more readable.  First we
                              # reset these, in case we use this
                              # object to make more than one flag.
        if code:
            code_re = re.search(r'^(\d{1,2}\.\d)?([bgkrwy]{2,6})?(?:-(\d))?$', code)
        if code and code_re:
            self.flag_code = code_re.group(1)
            self.color_code = code_re.group(2)
            try:
                self.variant = int(code_re.group(3))
                if self.verbose:
                    print(f'variant: {self.variant}', file=sys.stderr)
                user_variant = True
            except TypeError:
                pass  # or user_variant = False again
        if self.flag_code and self.color_code:
            # When the user gives a full flag code, checking if it is
            # known makes no sense.
            self.avoid_known = False
        user_flag_code = bool(self.flag_code)
        user_colors = bool(self.color_code)
        give_up_countdown = 50  # This is mostly a safety
                                # measure. Don’t hang if we can’t find
                                # a good flag code, but rather give up
                                # after 50 tries.
        if self.verbose:
            print(f'colors: {user_colors}', file=sys.stderr)
        for dummy_count in range(give_up_countdown):
            # It is concievable that we don’t find an unknown flag.
            if not user_flag_code:
                # Actually reset the flag code when trying again (known flag)
                self.flag_code = None
            if not user_colors:
                self.color_code = None
            flag_codes = self.getFlagCodes()
            long_flag_code = flag_codes[0]
            color_count = flag_codes[1]
            self.getColors(color_count)
            dummy_rgb, dummy_hsl, dummy_pm, c_names, c_codes = zip(*self.colors)
            for cn in range(flag_codes[1]):
                long_flag_code += c_codes[cn]
            if self.verbose:
                print(f'long code: {long_flag_code}', file=sys.stderr)
            if long_flag_code in self.known_flags:
                # print(
                #    'long_flag_code: {}'.format(long_flag_code),
                #    file=sys.stderr)
                if self.verbose:
                    print(
                        _('That is the flag of {}').format(
                            self.known_flags[long_flag_code]), file=sys.stderr)
                if avoid_known and not (user_flag_code and user_colors):
                    # random.shuffle(colors_list)  # Re-shuffle the colors. Maybe.
                    continue # So try again
            # Still here: either a new flag or we don’t care.
            # self.description = flag_codes[3].format(*c_names)
            try:
                self.description = descriptions[flag_codes[0]]
            except KeyError:
                self.description = None
            else:
                self.description = Template(self.description).render(color=c_names)
            if not user_variant:
                self.variant = 0
                if self.fuzz_variant:
                    try:
                        variants = flag_codes[2]
                        self.variant = random.choices(
                            range(len(variants)), variants, k=1)[0]
                        if self.verbose:
                            print(f'variant: -{self.variant}', file=sys.stderr)
                    except TypeError:
                        pass  # No variants given. We tried to get len(None)
            break # or return


    def writeSvgFlag(self, out_file):
        self.drawing.saveSvg(out_file)


    def writePngFlag(self, out_file):
        self.drawing.savePng(out_file)


    def makeFlag(self, code=None, avoid_known=False):
        if self.fuzz_length:
            self.fuzzLength()
        if self.verbose:
            print(f'Size: {self.length}×{self.width}', file=sys.stderr)
        self.setFlagInfo(code, avoid_known)
        self.makeFlagFromCode()
