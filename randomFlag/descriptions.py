#!/usr/bin/python
# -*- mode: Python ; coding: utf-8 -*-
#
# This is part of the random flag generator
#
# These ar the descriptions. They were getting distracting in the main
# file They hav to be kept in line with the flag_generator_list and
# the actual flags generated below that in the main file.
#
# © 2020–2023 Roland Sieker <ospalh@gmail.com>

from .gtext import _

descriptions = {
    '1.1': _('A flag with quadrants of {{ color[0] }}, {{ color[1] }}, {{ color[2] }} and {{ color[3] }}'),
    '1.2': _('A horizontal bicolor flag with {{ color[0] }} and {{ color[1] }} stripes'),
    '1.3': _('A vertical bicolor flag with {{ color[0] }} and {{ color[1] }} stripes'),
    '1.4': _('A 2×2 checkerd flag in {{ color[0] }} and {{ color[1] }}'),
    '1.5': _('A flag with quadrants of {{ color[0] }}, {{ color[1] }}, {{ color[1] }} and {{ color[2] }}'),
    '1.6': _('A flag with quadrants of {{ color[0] }}, {{ color[1] }}, {{ color[2] }} and {{ color[0] }}'),
    '1.7': _('A horizontal bicolor flag of {{ color[1] }} and {{ color[2] }} with a {{ color[0] }} hoist-side stripe'),
    '1.8': _('A horizontal bicolor flag of {{ color[1] }} and {{ color[2] }} with a {{ color[0] }} canton'),
    '2.1': _('A horizontal tricolor flag with {{ color[0] }}, {{ color[1] }} and {{ color[2] }} stripes'),
    '2.2': _('A horizontal triband flag with {{ color[0] }}, {{ color[1] }} and {{ color[0] }} stripes'),
    '3.1': _('A vertical tricolor flag with {{ color[0] }}, {{ color[1] }} and {{ color[2] }} stripes'),
    '3.2': _('A vertical triband flag with {{ color[0] }}, {{ color[1] }} and {{ color[0] }} stripes'),
    '4.1': _('A quadrant flag of {{ color[0] }}, {{ color[1] }}, {{ color[2] }} and {{ color[3] }} with a {{ color[4] }} cros'),
    '4.2': _('A horizontal bicolor flag of {{ color[0] }} and {{ color[1] }} with a {{ color[2] }} cros'),  # 3
    '4.3': _('A 2×2 checkerd flag in {{ color[0] }} and {{ color[1] }} with a {{ color[2] }} cros'),
    '4.4': _('A vertical bicolor flag of {{ color[0] }} and {{ color[1] }} with a {{ color[2] }} cros'),
    '4.5': _('A {{ color[0] }} flag with a {{ color[1] }} cros'),
    '5.1': _('A quadrant flag of {{ color[0] }}, {{ color[1] }}, {{ color[2] }} and {{ color[3] }} with a {{ color[4] }} cros with a {{ color[5] }} border'),  # 0
    '5.2': _('A horizontal bicolor flag of {{ color[0] }} and {{ color[1] }} with a {{ color[2] }} cros with a {{ color[3] }} border'),  # 3
    '5.3': _('A 2×2 checkerd flag in {{ color[0] }} and {{ color[1] }} with a {{ color[2] }} cros with a {{ color[3] }} border'),
    '5.4': _('A vertical bicolor flag of {{ color[0] }} and {{ color[1] }} with a {{ color[2] }} cros with a {{ color[3] }} border'),
    '5.5': _('A {{ color[0] }} flag with a {{ color[1] }} cros with a {{ color[2] }} border'),
    '5.6': _('A {{ color[0] }} flag with just a {{ color[1] }} border around a {{ color[0] }} cros'),
    '6.1': _('A horizontal tricolor flag with {{ color[0] }}, {{ color[1] }} and {{ color[2] }} stripes and a {{ color[3] }} hoist side triangle'),
    '6.2': _('A horizontal triband flag with {{ color[0] }}, {{ color[1] }}, {{ color[0] }} stripes and a {{ color[2] }} hoist side triangle'),
    '6.3': _('A {{ color[0] }} flag with a {{ color[1] }} hoist side triangle'),  # 3
    '6.4': _('A horizontal triband flag with {{ color[0] }}, {{ color[1] }}, {{ color[0] }} stripes, where the {{ color[1] }} stripe expands into a hoist side triangle'),
    '6.5': _('A horizontal triband flag with {{ color[0] }}, {{ color[1] }}, {{ color[0] }} stripes, where the {{ color[1] }} stripe is partially obscured by a {{ color[0] }} hoist side triangle'),  # 0
    '7.1': _('A horizontal bicolor flag with {{ color[0] }} and {{ color[1] }} stripes and a {{ color[2] }} hoist side triangle'),
    '8.1': _('A flag with five horizontal stripes of {{ color[0] }}, {{ color[1] }}, {{ color[2] }}, {{ color[1] }} and {{ color[0] }}'),
    '8.2': _('A {{ color[0] }} flag with two {{ color[1] }} horizontal stripes with a {{ color[0] }} stripe between them'),
    '8.3': _('A {{ color[0] }} flag with one {{ color[1] }} and one {{ color[2] }} horizontal stripe, separated by a {{ color[0] }} stripe'),
    '9.1': _('A horizontal tricolor flag with {{ color[0] }}, {{ color[1] }} and {{ color[2] }} stripes and a {{ color[3] }} disk'),
    '9.2': _('A horizontal triband flag with {{ color[0] }}, {{ color[1] }} and {{ color[0] }} stripes and a {{ color[2] }} disk'),
    '9.3': _('A {{ color[0] }} flag with a {{ color[1] }} disk'),
    '10.1': _('A flag quartert diagonally with {{ color[0] }} (top), {{ color[1] }} (hoist), {{ color[2] }} (fly) and {{ color[3] }} (bottom) triangles'),
    '10.2': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[1] }} (bottom fly) halves'),
    '10.3': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[1] }} (bottom hoist) halves'),
    '10.4': _('A flag quartert diagonally with {{ color[0] }} (top & bottom) and {{ color[1] }} (hoist & fly) triangles'),
    '10.5': _('A flag quartert diagonally with {{ color[0] }} (top & bottom), {{ color[1] }} (hoist) and {{ color[2] }} (fly) triangles'),
    '10.6': _('A flag quartert diagonally with {{ color[0] }} (top), {{ color[1] }} (hoist & fly) and {{ color[2] }} (bottom) triangles'),
    '11.1': _('A flag quartert diagonally with {{ color[0] }} (top), {{ color[1] }} (hoist), {{ color[2] }} (fly) and {{ color[3] }} (bottom) triangles and a {{ color[4] }} diagonal cros'),  # 1
    '11.2': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[1] }} (bottom fly) halves and a {{ color[2] }} diagonal cros'),  # 0
    '11.3': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[1] }} (bottom hoist) halves and a {{ color[2] }} diagonal cros'),  # 0
    '11.4': _('A flag quartert diagonally with {{ color[0] }} (top & bottom) and {{ color[1] }} (hoist & fly) triangles and a {{ color[2] }} diagonal cros'),
    '11.5': _('A flag quartert diagonally with {{ color[0] }} (top & bottom), {{ color[1] }} (hoist) and {{ color[2] }} (fly) triangles and a {{ color[3] }} diagonal cros'),  # 0
    '11.6': _('A flag quartert diagonally with {{ color[0] }} (top), {{ color[1] }} (hoist & fly) and {{ color[2] }} (bottom) triangles and a {{ color[3] }} diagonal cros'),  # 1
    '11.7': _('A {{ color[0] }} flag with a {{ color[1] }} diagonal cros'),
    '12.1': _('A horizontal tricolor flag with {{ color[0] }}, {{ color[1] }} and {{ color[2] }} stripes with a small {{ color[3] }} hoist side triangle and a larger {{ color[4] }} chevron'),  # 2
    '12.2': _('A horizontal tribar flag with {{ color[0] }}, {{ color[1] }} and {{ color[0] }} stripes with a small {{ color[2] }} hoist side triangle and a larger {{ color[3] }} chevron'),  # 2
    '12.3': _('A {{ color[0] }} flag with a small {{ color[1] }} hoist side triangle and a larger {{ color[2] }} chevron'),
    '12.4': _('A horizontal tricolor flag with {{ color[0] }}, {{ color[1] }} and {{ color[2] }} stripes with a small {{ color[3] }} hoist side triangle and a larger {{ color[1] }} chevron; the chevron and the middle stripe form a pall or lying Y-shpape'),  # 1
    '12.5': _('A horizontal tribar flag with {{ color[0] }}, {{ color[1] }} and {{ color[0] }} stripes with a small {{ color[2] }} hoist side triangle and a larger {{ color[1] }} chevron; the chevron and the middle stripe form a pall or lying Y-shpape'),  # 3
    '12.6': _('A horizontal tribar flag with {{ color[0] }}, {{ color[1] }} and {{ color[0] }} stripes with a small hoist side triangle, also {{ color[0] }}, and a larger {{ color[1] }} chevron; the chevron and the middle stripe form a pall or lying Y-shpape'),  # 2
    '12.7': _('A {{ color[0] }} flag with a larger {{ color[1] }} hoist side triangle partially obscured by a smaller {{ color[0] }} triangle. What remains of the {{ color[1] }} triangle formes a boomerang shape'),  # 0
    '13.1': _('A horizontal bicolor flag with {{ color[0] }} and {{ color[1] }} stripes with a smaller {{ color[2] }} and a larger {{ color[3] }} chevron'),  # 2
    '14.1': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[2] }} (bottom fly) halves and a {{ color[1] }} line or bend between the halves'),
    '14.2': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[2] }} (bottom hoist) halves and a {{ color[1] }} line or bend between the halves'),
    '14.3': _('A {{ color[0] }} flag with a {{ color[1] }} bend sinister, a diagonal line from the top fly corner to the bottom hoist corner'),
    '14.4': _('A {{ color[0] }} flag with a {{ color[1] }} bend, a diagonal line from the top hoist corner to the bottom fly corner'),
    '15.1': _('A flag with quadrants of {{ color[0] }}, {{ color[1] }}, {{ color[2] }} and {{ color[3] }} with a {{ color[4] }} lozenge in the center'),  # 0
    '15.2': _('A horizontal bicolor flag with {{ color[0] }} and {{ color[1] }} stripes with a {{ color[2] }} lozenge in the center'),  # 1
    '15.3': _('A vertical bicolor flag with {{ color[0] }} and {{ color[1] }} stripes with a {{ color[2] }} lozenge in the center'),  # 0
    '15.4': _('A 2×2 checkerd flag in {{ color[0] }} and {{ color[1] }} with a {{ color[2] }} lozenge in the center'),  # 0
    '15.5': _('A flag with quadrants of {{ color[0] }}, {{ color[1] }}, {{ color[1] }} and {{ color[2] }} with a {{ color[3] }} lozenge in the center'),  # 0
    '15.6': _('A flag with quadrants of {{ color[0] }}, {{ color[1] }}, {{ color[2] }} and {{ color[0] }} with a {{ color[3] }} lozenge in the center'),  # 0
    '15.7': _('A {{ color[0] }} flag with a {{ color[1] }} lozenge in the center'),
    '16.1': _('A horizontal tricolor flag with {{ color[0] }}, {{ color[1] }} and {{ color[2] }} stripes and a {{ color[3] }} hoist-side bar'),
    '16.2': _('A horizontal tribar flag with {{ color[0] }}, {{ color[1] }} and {{ color[0] }} stripes and a {{ color[2] }} hoist-side bar'),  # 2
    '16.3': _('A horizontal tricolor flag with {{ color[0] }}, {{ color[1] }} and {{ color[2] }} stripes and a {{ color[1] }} hoist-side bar; the middle stripe and the bar form a lying T shape'),  # 4
    '16.4': _('A horizontal tribar flag with {{ color[0] }}, {{ color[1] }} and {{ color[0] }} stripes and a {{ color[1] }} hoist-side bar; the middle stripe and the bar form a lying T shape'),  # 0
    '17.1': _('A horizontal bicolor flag with {{ color[0] }} and {{ color[3] }} stripes with a {{ color[1] }} and {{ color[2] }} disk, split horizontally'),  # 0
    '17.2': _('A horizontal bicolor flag with {{ color[0] }} and {{ color[1] }} stripes with a {{ color[1] }} and {{ color[0] }} disk, split horizontally'),  # 0
    '17.3': _('A horizontal bicolor flag with {{ color[0] }} and {{ color[2] }} stripes with a {{ color[1] }} disk'),
    '17.4': _('A {{ color[0] }} flag with a {{ color[1] }} and {{ color[2] }} disk, split horizontally'),
    '18.1': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[2] }} (bottom hoist) halves and a {{ color[1] }} line or bend in the other diagonal'),
    '18.2': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[2] }} (bottom fly) halves and a {{ color[1] }} line or bend in the other diagonal'),
    '19.1': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[3] }} (bottom fly) halves and two lines or bends sinister between the halves in {{ color[1] }} and {{ color[2] }}'),
    '19.2': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[3] }} (bottom hoist) halves and two lines or bends between the halves in {{ color[1] }} and {{ color[2] }}'),
    '19.3': _('A {{ color[0] }} flag with two bends sinister, diagonal lines from the top fly corner to the bottom hoist corner in {{ color[1] }} and {{ color[2] }}'),
    '19.4': _('A {{ color[0] }} flag with two bends, diagonal lines from the top hoist corner to the bottom fly corner, in {{ color[1] }} and {{ color[2] }}'),
    '19.5': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[3] }} (bottom fly) halves with two bends sinister, diagonal lines from the top fly corner to the bottom hoist corner, in {{ color[1] }} and {{ color[2] }} with a gap between them in {{ color[0] }} and {{ color[3] }}'),
    '19.6': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[3] }} (bottom hoist) halves with two bends, diagonal lines from the top hoist corner to the bottom fly corner, in {{ color[1] }} and {{ color[2] }} with a gap between them in {{ color[0] }} and {{ color[3] }}'),
    '19.7': _('A {{ color[0] }} flag with two bends sinister, diagonal lines from the top fly corner to the bottom hoist corner in {{ color[1] }} and {{ color[2] }} with a {{ color[0] }} gap between them'),
    '19.8': _('A {{ color[0] }} flag with two bends, diagonal lines from the top hoist corner to the bottom fly corner in {{ color[1] }} and {{ color[2] }} with a {{ color[0] }} gap between them'),
    '20.1': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[3] }} (bottom fly) halves with a {{ color[2] }} line or bend sinister between the halves, separated from them with two more lines in {{ color[1] }}'),
    '20.2': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[3] }} (bottom hoist) halves with a {{ color[2] }} line or bend between the halves, separated from them with two more lines in {{ color[1] }}}'),
    '20.3': _('A {{ color[0] }} flag with three bends sinister, diagonal lines from the top fly corner to the bottom hoist in {{ color[1] }}, {{ color[2] }} and {{ color[1] }}'),
    '20.4': _('A {{ color[0] }} flag with three bends, diagonal lines from the top hoist corner to the bottom fly corner in in {{ color[1] }}, {{ color[2] }} and {{ color[1] }}'),
    '20.5': _('A {{ color[0] }} flag with two bends sinister, diagonal lines from the top fly corner to the bottom hoist corner in {{ color[1] }} with a {{ color[0] }} gap between them'),
    '20.6': _('A {{ color[0] }} flag with two bends, diagonal lines from the top hoist corner to the bottom fly corner, in {{ color[1] }} with a {{ color[0] }} gap between them'),
    '21.1': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[3] }} (bottom hoist) halves with two lines or bends sinister along the other diagonal in {{ color[1] }} and {{ color[2] }}'),
    '21.2': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[3] }} (bottom fly) halves with two lines or bends along the other diagonal in {{ color[1] }} and {{ color[2] }}'),
    '21.3': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[3] }} (bottom hoist) halves with two lines or bends sinister along the other diagonal in {{ color[1] }} and {{ color[2] }} with a {{ color[0] }}-{{ color[3] }} gap between them'),
    '21.4': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[3] }} (bottom fly) halves with two lines or bends along the other diagonal in {{ color[1] }} and {{ color[2] }} with a {{ color[0] }}-{{ color[3] }} gap between them'),
    '21.5': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[2] }} (bottom hoist) halves with two lines or bends sinister along the other diagonal in {{ color[1] }} with a {{ color[0] }}-{{ color[2] }} gap between them'),
    '21.6': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[2] }} (bottom fly) halves with two lines or bends along the other diagonal in {{ color[1] }} with a {{ color[0] }}-{{ color[2] }} gap between them'),
    '22.1': _('A flag split diagonally into {{ color[0] }} (top fly) and {{ color[3] }} (bottom hoist) halves with three lines or bends sinister along the other diagonal in {{ color[1] }}, {{ color[2] }} and {{ color[1] }}'),
    '22.2': _('A flag split diagonally into {{ color[0] }} (top hoist) and {{ color[3] }} (bottom fly) halves with three lines or bends along the other diagonal in {{ color[1] }}, {{ color[2] }} and {{ color[1] }}'),
    '23.1': _('A flag with five horizontal stripes of {{ color[0] }}, {{ color[1] }}, {{ color[2] }}, {{ color[1] }} and {{ color[0] }} and a {{ color[3] }} hoist-side bar'),
    '23.2': _('A {{ color[0] }} flag with two {{ color[1] }} horizontal stripes, separated by a {{ color[0] }} stripe and a {{ color[2] }} hoist-side bar'),
    '23.3': _('A {{ color[0] }} flag with one {{ color[1] }} and one {{ color[2] }} horizontal stripe, separated by a {{ color[0] }} stripe and a {{ color[3] }} hoist-side bar'),
    '23.4': _('A flag with five horizontal stripes of {{ color[0] }}, {{ color[1] }}, {{ color[2] }}, {{ color[1] }} and {{ color[0] }} and a {{ color[2] }} hoist-side bar, joind to the central stripe'),
    '23.5': _('A flag with five horizontal stripes of {{ color[0] }}, {{ color[1] }}, {{ color[2] }}, {{ color[1] }} and {{ color[0] }} and a {{ color[1] }} hoist-side bar, joind to the second and fourth stripes'),
    '23.6': _('A {{ color[0] }} flag with two {{ color[1] }} horizontal stripes and a {{ color[0] }} hoist-side bar. The {{ color[0] }} parts form an E shape'),
    '23.7': _('A {{ color[0] }} flag with two {{ color[1] }} horizontal stripes and a {{ color[1] }} hoist-side bar, joined to the two stripes'),
    '24.1': _('A flag with five horizontal stripes of {{ color[0] }}, {{ color[1] }}, {{ color[2] }}, {{ color[1] }} and {{ color[0] }} and a {{ color[3] }} hoist-side triangle'),
    '24.2': _('A {{ color[0] }} flag with two {{ color[1] }} horizontal stripes, separated by a {{ color[0] }} stripe and a {{ color[2] }} hoist-side triangle'),
    '24.3': _('A {{ color[0] }} flag with one {{ color[1] }} and one {{ color[2] }} horizontal stripe, separated by a {{ color[0] }} stripe and a {{ color[3] }} hoist-side triangle'),
    '24.4': _('A flag with five horizontal stripes of {{ color[0] }}, {{ color[1] }}, {{ color[2] }}, {{ color[1] }} and {{ color[0] }} and a {{ color[2] }} hoist-side triangle, joind to the central stripe'),
    '24.5': _('A flag with five horizontal stripes of {{ color[0] }}, {{ color[1] }}, {{ color[2] }}, {{ color[1] }} and {{ color[0] }} and a {{ color[1] }} hoist-side triangle, joind to the second and fourth stripes'),
    '24.6': _('A {{ color[0] }} flag with two {{ color[1] }} horizontal stripes and a {{ color[0] }} hoist-side triangle, joined to the {{ color[0] }} stripes.'),
    '24.7': _('A {{ color[0] }} flag with two {{ color[1] }} horizontal stripes and a {{ color[1] }} hoist-side triangle, joined to the two {{ color[1] }} stripes'),
    '25.1': _('A flag of five oblique bands or rays radiating from the bottom hoist corner of {{ color[0] }} (along the hoist), {{ color[1] }} (from the bottom hoist corner to the middle of the top edge) {{ color[2] }} (from the bottom hoist corner to the top fly corner) {{ color[3] }} (from the bottom hoist corner to the middle of the fly edge) and {{ color[4] }} (along the bottom edge).'),
    '25.2': _('A flag of five oblique bands or rays radiating from the bottom hoist corner of {{ color[0] }} (along the hoist), {{ color[1] }} (from the bottom hoist corner to the middle of the top edge) {{ color[2] }} (from the bottom hoist corner to the top fly corner) {{ color[1] }} (from the bottom hoist corner to the middle of the fly edge) and {{ color[0] }} (along the bottom edge).'),
    '25.3': _('A flag of five oblique bands or rays radiating from the bottom hoist corner of {{ color[0] }} (along the hoist), {{ color[1] }} (from the bottom hoist corner to the middle of the top edge) {{ color[0] }} (from the bottom hoist corner to the top fly corner) {{ color[1] }} (from the bottom hoist corner to the middle of the fly edge) and {{ color[0] }} (along the bottom edge).'),
    '25.4': _('A flag of five oblique bands or rays radiating from the bottom hoist corner of {{ color[0] }} (along the hoist), {{ color[1] }} (from the bottom hoist corner to the middle of the top edge) {{ color[0] }} (from the bottom hoist corner to the top fly corner) {{ color[2] }} (from the bottom hoist corner to the middle of the fly edge) and {{ color[0] }} (along the bottom edge).'),
    '25.5': _('A {{ color[2] }} flag with two oblique bands or rays radiating from the bottom hoist corner of {{ color[0] }} (along the hoist) and {{ color[1] }} (from the bottom hoist corner to the middle of the top edge).'),
    '25.6': _('A {{ color[2] }} flag with two oblique bands or rays radiating from the bottom hoist corner of  {{ color[1] }} (from the bottom hoist corner to the middle of the fly edge) and {{ color[0] }} (along the bottom edge).'),
    '26.1': _('A flag of five oblique bands or rays radiating from the top hoist corner of {{ color[0] }} (along the top edge), {{ color[1] }} (from the top hoist corner to the middle of the fly edge), {{ color[2] }} (from the top hoist corner to the bottom fly corner) {{ color[3] }} (from the top hoist corner to the middle of the bottom edge) and {{ color[4] }} (along the hoist).'),
    '26.2': _('A flag of five oblique bands or rays radiating from the top hoist corner of {{ color[0] }} (along the top edge), {{ color[1] }} (from the top hoist corner to the middle of the fly edge), {{ color[2] }} (from the top hoist corner to the bottom fly corner) {{ color[1] }} (from the top hoist corner to the middle of the bottom edge) and {{ color[0] }} (along the hoist).'),
    '26.3': _('A flag of five oblique bands or rays radiating from the top hoist corner of {{ color[0] }} (along the top edge), {{ color[1] }} (from the top hoist corner to the middle of the fly edge), {{ color[0] }} (from the top hoist corner to the bottom fly corner) {{ color[1] }} (from the top hoist corner to the middle of the bottom edge) and {{ color[0] }} (along the hoist).'),
    '26.4': _('A flag of five oblique bands or rays radiating from the top hoist corner of {{ color[0] }} (along the top edge), {{ color[1] }} (from the top hoist corner to the middle of the fly edge), {{ color[0] }} (from the top hoist corner to the bottom fly corner) {{ color[2] }} (from the top hoist corner to the middle of the bottom edge) and {{ color[0] }} (along the hoist).'),
    '26.5': _('A {{ color[2] }} flag with two oblique bands or rays radiating from the top hoist corner of {{ color[0] }} (along the hoist) and {{ color[1] }} (from the top hoist corner to the middle of the bottom edge).'),
    '26.6': _('A {{ color[2] }} flag with two oblique bands or rays radiating from the top hoist corner of  {{ color[0] }} (along the top edge) and {{ color[1] }} (from the top hoist corner to the middle of the fly edge).'),
}
