# -*- mode: Python ; coding: utf-8 -*-
#
# © 2020–2021 Roland Sieker <ospalh@gmail.com>
#
# License: GNU AGPL, version 3 or later;
# http://www.gnu.org/copyleft/agpl.html

"""The init file to run the app"""

import argparse

from .random_flag import RandomFlag
from .gtext import _

__version__ = "5.0.0"


__all__ = ["RandomFlag"]

def randomFlag(argv=None):
    if not argv:
        argv = ['randomFlag']
    parser = argparse.ArgumentParser(description=_("Create a random flag"))
    parser.add_argument(
        '-p', '--png', action='store_true',
        help=_('Create a PNG file instead of an SVG file'))
    parser.add_argument(
        '-v', '--verbose', action='store_true',
        help=_('Print some debugging output'))
    parser.add_argument(
        '-q', '--quiet', action='store_true',
        help=_('Don’t print the description'))
    parser.add_argument(
        '-n', '--no-fuzz', dest='fuzz', action='store_false',
        help=_('Do not randomize the colors, the aspect ratio or the variant'))
    parser.add_argument(
        '-a', '--allow-known', dest='avoid_known', action='store_false',
        help=_('Do not skip flags similar to known flags'))
    parser.add_argument(
        '-c', '--code', type=str,
        help=_('Create flag for a given code.'))
    parser.add_argument(
        '-w', '--width', type=int, default=480,
        help=_('The width of the flag, that is, the top-to-bottom size. (The other size is the length)'))
    parser.add_argument('output', help=_('The output file.'))
    args = parser.parse_args()
    rflag = RandomFlag(args.width, args.verbose, args.fuzz)
    rflag.makeFlag(args.code, args.avoid_known)
    if not args.quiet:
        if rflag.description:
            print(rflag.description)
        else:
            print('(No description provided)')
    if args.png:
        rflag.writePngFlag(args.output)
    else:
        rflag.writeSvgFlag(args.output)
