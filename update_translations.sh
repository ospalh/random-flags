#!/bin/bash

# This is based on the file from mcomix3.

ROOTDIR="randomFlag"
VERSION=$(grep __version__ $ROOTDIR/__init__.py | choose -f \'\|\" 1 )
# MAINTAINER="NAME@HO.ST"

xgettext -LPython -orandomFlag.pot -p$ROOTDIR/messages/ -cTRANSLATORS \
    --from-code=utf-8 --package-name=randomFlag --package-version=${VERSION} \
    $ROOTDIR/*.py
#     --msgid-bugs-address=${MAINTAINER} \


for pofile in $ROOTDIR/messages/*/LC_MESSAGES/*.po
do
    # Merge message files with master template, no fuzzy matching (-N)
    msgmerge -U --backup=none ${pofile} $ROOTDIR/messages/randomFlag.pot
    # Compile translation, add "-f" to include fuzzy strings
    msgfmt ${pofile} -o ${pofile%.*}.mo
done
